use albedo_math::hyperspace_fuel_eq::{
    ship_fuel_left_almost_dry, ship_fuel_left_nominal, ship_mass_almost_dry, ship_mass_nominal,
};
use albedo_utils::consts::fsd::FSD_CONSTS;
use anyhow::anyhow;
use clap::{Args, Subcommand};

#[derive(Args)]
pub(crate) struct FuelCommand {
    #[clap(subcommand)]
    command: FuelSubcommands,
}

impl FuelCommand {
    pub(crate) fn run(&self) -> crate::Result {
        self.command.run()
    }
}

#[derive(Subcommand)]
enum FuelSubcommands {
    Left(LeftCommand),
}

impl FuelSubcommands {
    fn run(&self) -> crate::Result {
        match self {
            FuelSubcommands::Left(command) => command.run(),
        }
    }
}

#[derive(Args)]
struct LeftCommand {
    #[clap(required_unless_present("size"))]
    fsd_size: Option<u8>,
    #[clap(required_unless_present("class"))]
    fsd_class: Option<char>,
    #[clap(required_unless_present("max-range"))]
    maximum_range: Option<f64>,
    #[clap(required_unless_present("cur-range"))]
    current_range: Option<f64>,
    #[clap(short, long)]
    size: Option<u8>,
    #[clap(short, long)]
    class: Option<char>,
    #[clap(short, long)]
    max_range: Option<f64>,
    #[clap(short = 'r', long)]
    cur_range: Option<f64>,
    #[clap(short, long)]
    almost_dry: bool,
}

impl LeftCommand {
    fn run(&self) -> crate::Result {
        let size =
            self.size
                .or(self.fsd_size)
                .ok_or(anyhow!("Unexpected at {}:{}", file!(), line!()))?;
        let class = self
            .class
            .or(self.fsd_class)
            .ok_or(anyhow!("Unexpected at {}:{}", file!(), line!()))?
            .to_ascii_uppercase();
        let max_range = self.max_range.or(self.maximum_range).ok_or(anyhow!(
            "Unexpected at {}:{}",
            file!(),
            line!()
        ))?;
        let cur_range = self.cur_range.or(self.current_range).ok_or(anyhow!(
            "Unexpected at {}:{}",
            file!(),
            line!()
        ))?;
        let spec = (size, class).try_into()?;
        let fuel_left = if self.almost_dry {
            ship_fuel_left_almost_dry(spec, max_range, cur_range)
        } else {
            ship_fuel_left_nominal(spec, max_range, cur_range)
        };
        #[cfg(debug_assertions)]
        {
            let fuel_consumed = FSD_CONSTS.max_fuel_consumption(&spec);
            eprintln!(
                "current: {:.3}T",
                if self.almost_dry {
                    ship_mass_almost_dry(spec, max_range, cur_range)
                } else {
                    ship_mass_nominal(spec, fuel_consumed, cur_range)
                }
            );
            eprintln!(
                "minimal: {:.3}T",
                ship_mass_nominal(spec, fuel_consumed, max_range)
            );
        }
        println!("{:.3}T", fuel_left);
        Ok(())
    }
}
