mod fuel;

use crate::fuel::FuelCommand;
use anyhow;
use clap::{Parser, Subcommand};

type Result = anyhow::Result<()>;

#[derive(Parser)]
#[clap(author, version, about)]
struct Cli {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand)]
enum Command {
    Fuel(FuelCommand),
}

impl Command {
    fn run(&self) -> Result {
        match self {
            Command::Fuel(command) => command.run(),
        }
    }
}

fn main() -> Result {
    let cli: Cli = Cli::parse();
    cli.command.run()
}
