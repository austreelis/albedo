//! albedo modules

pub use cores::*;

pub mod cores;

#[derive(Copy, Clone)]
pub struct Module<K, U> {
    pub damage: f64,
    pub kind: K,
    pub upgrade: U,
}

impl<K, U> Default for Module<K, U>
where
    K: Default,
    U: Default,
{
    #[inline(always)]
    fn default() -> Self {
        Module {
            damage: 0.0,
            kind: Default::default(),
            upgrade: Default::default(),
        }
    }
}
