use albedo_math::hyperspace_fuel_eq::jump_dist;
use albedo_traits::Mass;
use albedo_utils::consts::fsd::*;
use albedo_utils::types::core_modules::FSDSpec;

use crate::Module;

#[derive(Copy, Clone, Eq, PartialEq, Default)]
pub struct FrameShiftDrive<const S: u8, const C: char>;

pub trait FSD {
    /// The FSD's size and class.
    const SPEC: FSDSpec;

    /// The FSD's mass  in Tons.
    const MASS: f64;

    /// The FSD's optimal mass in Tons.
    const OPTIMIZED_MASS: f64;

    /// The FSD's max fuel consumption per jump.
    const MAX_FUEL_CONSUMPTION: f64;

    fn max_range(&self, ship_mass: f64) -> f64 {
        jump_dist(Self::SPEC, Self::MAX_FUEL_CONSUMPTION, ship_mass)
    }
}

impl<F, U> Mass<f64> for Module<F, U>
where
    Module<F, U>: FSD,
{
    const MASS: f64 = <Module<F, U> as FSD>::MASS;

    #[inline]
    fn mass(&self) -> f64 {
        <Self as Mass<f64>>::MASS
    }
}

// TODO: We should use a macro really
// TODO: Implement remaining sizes & classes

impl FSD for Module<FrameShiftDrive<2, 'D'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(2, 'D' as u8) };
    const MASS: f64 = FSD_2D_MASS;
    const OPTIMIZED_MASS: f64 = FSD_2D_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_2D_MAX_FUEL_CONSUMPTION;
}

impl FSD for Module<FrameShiftDrive<2, 'A'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(2, 'A' as u8) };
    const MASS: f64 = FSD_2A_MASS;
    const OPTIMIZED_MASS: f64 = FSD_2A_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_2A_MAX_FUEL_CONSUMPTION;
}

impl FSD for Module<FrameShiftDrive<3, 'D'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(3, 'D' as u8) };
    const MASS: f64 = FSD_3D_MASS;
    const OPTIMIZED_MASS: f64 = FSD_3D_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_3D_MAX_FUEL_CONSUMPTION;
}

impl FSD for Module<FrameShiftDrive<3, 'A'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(3, 'A' as u8) };
    const MASS: f64 = FSD_3A_MASS;
    const OPTIMIZED_MASS: f64 = FSD_3A_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_3A_MAX_FUEL_CONSUMPTION;
}

impl FSD for Module<FrameShiftDrive<4, 'D'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(4, 'D' as u8) };
    const MASS: f64 = FSD_3A_MASS;
    const OPTIMIZED_MASS: f64 = FSD_3A_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_3A_MAX_FUEL_CONSUMPTION;
}

impl FSD for Module<FrameShiftDrive<4, 'A'>, ()> {
    const SPEC: FSDSpec = unsafe { FSDSpec::new_unchecked(4, 'A' as u8) };
    const MASS: f64 = FSD_4D_MASS;
    const OPTIMIZED_MASS: f64 = FSD_4D_OPTIMIZED_MASS;
    const MAX_FUEL_CONSUMPTION: f64 = FSD_4D_MAX_FUEL_CONSUMPTION;
}
