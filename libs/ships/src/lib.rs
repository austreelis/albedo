//! albedo ships
//!

use albedo_modules::{Hull, FSD};
use albedo_traits::Mass;
pub use ships::*;

mod ships;

pub struct ShipCore<C, O, M, U, W> {
    pub cores: C,
    pub optionals: O,
    pub militaries: M,
    pub utilities: U,
    pub weapons: W,
}

impl<C, O, M, U, W> Mass<f64> for ShipCore<C, O, M, U, W>
where
    C: Mass,
    O: Mass,
    M: Mass,
    U: Mass,
    W: Mass,
{
    const MASS: f64 = C::MASS + O::MASS + M::MASS + U::MASS + W::MASS;

    #[inline]
    fn mass(&self) -> f64 {
        self.cores.mass()
            + self.optionals.mass()
            + self.militaries.mass()
            + self.utilities.mass()
            + self.weapons.mass()
    }
}

pub(crate) struct Cores<HL, FD> {
    hull: HL,
    fsd: FD,
}

pub trait Ship {
    type Hull: Hull;
    type FSD: FSD;

    const NAME: &'static str;
    /// The number of seats for multi-crew.
    const CREW_SEATS: u64;
    /// Maximum thruster speed with current mass equal to thruster's optimal mass.
    const TOP_SPEED: f64;
    /// Maximum boost speed with current mass equal to thruster's optimal mass.
    const BOOST_SPEED: f64;
    /// Thruster speed modifier with no distributor pips to [`eng`](albedo-types::Pips::eng).
    const MIN_THRUST: f64;
    const BOOST_COST: f64;
    const MANOEUVRABILITY: u64;
    const PITCH_SPEED: f64;
    const ROLL_SPEED: f64;
    const YAW_SPEED: f64;
    const MIN_PITCH_SPEED: f64;
    const SHIELDS: u64;
    const ARMOUR: u64;
    const ARMOUR_HARDNESS: u64;
    const HEAT_CAPACITY: u64;
    const MIN_HEAT_DISSIPATION: f64;
    const MAX_HEAT_DISSIPATION: f64;
    const MASS: f64;
    const MASS_LOCK: u64;
    const FUEL_COST: u64;
    const FUEL_RESERVE: f64;

    /// Get this ship's hull module.
    fn hull(&self) -> &Self::Hull;

    fn fsd(&self) -> &Self::FSD;
}

#[derive(Default)]
pub struct ShipBuilder<C, O, M, U, W> {
    pub cores: Option<C>,
    pub optionals: Option<O>,
    pub militaries: Option<M>,
    pub utilities: Option<U>,
    pub weapons: Option<W>,
}

// TODO: Nested builder types
impl<C, O, M, U, W> TryFrom<ShipBuilder<C, O, M, U, W>> for ShipCore<C, O, M, U, W>
where
    ShipCore<C, O, M, U, W>: Ship,
{
    type Error = (); // TODO: Custom error type

    fn try_from(value: ShipBuilder<C, O, M, U, W>) -> Result<Self, Self::Error> {
        match (
            value.cores,
            value.optionals,
            value.militaries,
            value.utilities,
            value.weapons,
        ) {
            (Some(cores), Some(optionals), Some(militaries), Some(utilities), Some(weapons)) => {
                Ok(ShipCore {
                    cores,
                    optionals,
                    militaries,
                    utilities,
                    weapons,
                })
            }
            _ => Err(()),
        }
    }
}

impl<C, O, M, U, W> ShipCore<C, O, M, U, W>
where
    ShipCore<C, O, M, U, W>: Ship,
{
    pub fn with_cores(mut self, cores: C) -> Self {
        self.cores = cores;
        self
    }

    pub fn with_optionals(mut self, optionals: O) -> Self {
        self.optionals = optionals;
        self
    }

    pub fn with_militaries(mut self, militaries: M) -> Self {
        self.militaries = militaries;
        self
    }

    pub fn with_utilities(mut self, utilities: U) -> Self {
        self.utilities = utilities;
        self
    }

    pub fn with_weapons(mut self, weapons: W) -> Self {
        self.weapons = weapons;
        self
    }
}
impl<C, O, M, U, W> ShipCore<C, O, M, U, W>
where
    ShipCore<C, O, M, U, W>: Ship + Mass,
{
    pub fn max_range(&self) -> f64 {
        self.fsd().max_range(<Self as Mass>::MASS)
    }
}
