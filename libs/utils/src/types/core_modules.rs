use crate::consts::fsd::FSD_CONSTS;
use thiserror::Error;

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct FSDSpec {
    pub(crate) size: u8,
    pub(crate) class: u8,
}

impl FSDSpec {
    pub const unsafe fn new_unchecked(size: u8, class: u8) -> Self {
        FSDSpec { size, class }
    }

    #[inline(always)]
    pub const fn size(&self) -> u8 {
        self.size
    }

    #[inline(always)]
    pub const fn class(&self) -> u8 {
        self.class
    }

    #[inline]
    pub fn mass(&self) -> f64 {
        FSD_CONSTS.mass(self)
    }

    #[inline]
    pub fn max_fuel_consumption(&self) -> f64 {
        FSD_CONSTS.max_fuel_consumption(self)
    }

    #[inline]
    pub fn optimized_mass(&self) -> f64 {
        FSD_CONSTS.optimized_mass(self)
    }
}

#[derive(Error, Debug)]
pub enum FSDSpecConversionError {
    #[error("{0} is not a valid class for an FSD")]
    Class(char),
    #[error("{0} is not a valid size for an FSD")]
    Size(u8),
}

impl TryFrom<(u8, u8)> for FSDSpec {
    type Error = FSDSpecConversionError;

    fn try_from((size, class): (u8, u8)) -> Result<Self, Self::Error> {
        if size < 2 || size > 4 {
            Err(FSDSpecConversionError::Size(size))
        } else if class != 'A' as u8 && class != 'D' as u8 {
            Err(FSDSpecConversionError::Class(class as char))
        } else {
            Ok(FSDSpec { size, class })
        }
    }
}

impl TryFrom<(u8, char)> for FSDSpec {
    type Error = FSDSpecConversionError;

    fn try_from((size, class): (u8, char)) -> Result<Self, Self::Error> {
        if (class as u32) < 0x100 {
            (size, class as u8).try_into()
        } else {
            Err(FSDSpecConversionError::Class(class))
        }
    }
}
